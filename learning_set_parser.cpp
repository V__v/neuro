#include "learning_set_parser.h"

LearningSetParser::LearningSetParser(const std::string& learningSetFileName)
{
    m_inputFile.setFileName(learningSetFileName.c_str());
    if (m_inputFile.open(QIODevice::ReadOnly))
    {
        m_in = new QTextStream(&m_inputFile);
        QStringList splittedLine = m_in->readLine().split(" ");
        m_rows = splittedLine.at(0).toInt();
        m_columns = splittedLine.at(1).toInt();
    }
    else
    {
        throw std::runtime_error("Opening " + learningSetFileName + " was failured.\n");
    }
}

void LearningSetParser::Close()
{
    m_inputFile.close();
}

bool LearningSetParser::GetNextLearningData(NeuroNet::LearingData& data)
{
    if (!data.inputs.empty())
    {
        data.inputs.clear();
    }

    QString object;
    if (!m_in->atEnd())
    {
        QString line = m_in->readLine();
        data.objectClass = line.mid(m_rows * m_columns).toStdString();
        object = line.left(m_rows * m_columns);
        for (const auto& ch: object)
        {
            data.inputs.push_back(ch.digitValue());
        }
        return true;
    }
    else
    {
        return false;
    }
}

int LearningSetParser::GetLearningDataSize()
{
    return m_rows * m_columns;
}
