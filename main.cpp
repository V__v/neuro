#include <iostream>
#include <string>
#include <vector>
#include "neuro_net.h"
#include "learning_set_parser.h"

int main()
{
    try
    {
        std::vector<std::string> classes = {
            "1", "2", "3"
        };

        LearningSetParser parser("D:\\MyProjects\\neuro\\data\\coded_chars_for_lab2_(300)0.1.txt");
        NeuroNet net({parser.GetLearningDataSize()}, classes);

        NeuroNet::LearingData data;
        while(parser.GetNextLearningData(data))
        {
            net.Learn(data.inputs, data.objectClass);
        }

        net.Process(std::vector<double> {1,1,1,1,0,0,0,1,0,0,1,1,1,1,1,1,1,0,0,0,1,0,0,0,1,1,1,0});
        std::cout << "Recognized class: " << net.GetRecognizedClass() << std::endl;
    }
    catch (const std::runtime_error& ex)
    {
        std::cout << ex.what();
    }

    return 0;
}
