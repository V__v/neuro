#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <random>
#include <cmath>

class Neuron
{
public:
    Neuron(std::string neuronName);
    void SetWeights(std::vector<double> weights);
    void SetInputs(std::vector<double> inputs);
    std::vector<double> MulWeightsOnError(double error);
    void RandomizeWeightsIfNeed();
    double Out();
    double SavedOut();
    void SetError(double);
    double SavedError();
    std::string Name();
    void ChangeAllWeights();
private:
    double ActivationFunc(double x);
    int m_inputsCount = 0;
    std::string m_name;
    std::vector<double> m_weights;
    std::vector<double> m_inputs;
    double m_out;
    double m_error;
    bool m_weightsIsSet = false;
};
