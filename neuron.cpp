#include "neuron.h"

Neuron::Neuron(std::string neuronName)
{
    m_name = neuronName;
}

void Neuron::RandomizeWeightsIfNeed()
{
    if (m_weightsIsSet)
    {
        return;
    }
    m_weights.resize(m_inputs.size());

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(-0.5, 0.5);

    std::generate(m_weights.begin(), m_weights.end(), [&dist, &mt]{return dist(mt);});
    m_weightsIsSet = true;
}

void Neuron::SetWeights(std::vector<double> weights)
{
    m_weights = std::move(weights);
}

void Neuron::SetInputs(std::vector<double> inputs)
{
    m_inputs = std::move(inputs);
}

std::vector<double> Neuron::MulWeightsOnError(double error)
{
    std::vector<double> errorsMulOnWeight(m_weights.size());

    std::transform(m_weights.begin(), m_weights.end(),
                   errorsMulOnWeight.begin(), std::bind1st(std::multiplies<double>(), error));

    return errorsMulOnWeight;
}

double Neuron::Out()
{
    double summatorResult = 0;//std::inner_product(m_weights.begin(), m_weights.end(), m_inputs.begin(), 0);

    for (size_t i = 0; i < m_weights.size(); ++i)
    {
        summatorResult += m_weights[i] * m_inputs[i];
    }

    m_out = ActivationFunc(summatorResult);

    return m_out;
}

double Neuron::SavedOut()
{
    return m_out;
}

void Neuron::SetError(double error)
{
    m_error = error;
}

double Neuron::SavedError()
{
    return m_error;
}

double Neuron::ActivationFunc(double x)
{
    return 1.0 / (1.0 + exp(x*(-1)));
}

std::string Neuron::Name()
{
    return m_name;
}

void Neuron::ChangeAllWeights()
{
    for (int i = 0; i < m_weights.size(); ++i)
    {
        double delta = m_inputs[i] * m_error;
        m_weights[i] += delta;
    }
}
