#pragma once
#include "neuro_layer.h"

class NeuroNet
{
public:
    struct LearingData
    {
        std::vector<double> inputs;
        std::string objectClass;
    };

    NeuroNet(std::vector<int> neuronsCounts, std::vector<std::string> classes);
    void Process(std::vector<double> inputs);
    void Learn(std::vector<double>& inputs, const std::string& objectClass);
    std::string GetRecognizedClass();
private:
    void ChangeAllWeights();
    std::vector<NeuroLayer*> m_layers;
};
