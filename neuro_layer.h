#pragma once
#include "neuron.h"

class NeuroLayer
{
public:
    NeuroLayer(int neuronsCount);
    NeuroLayer(std::vector<std::string> classes);
    std::vector<double> Process(const std::vector<double>& inputs);
    std::vector<std::vector<double>> Learn(const std::string& objectClass);
    std::vector<std::vector<double>> Learn(const std::vector<std::vector<double>>& errors);
    void ChangeAllWeights();
    std::string GetRecognizedClass();
private:
    std::vector<Neuron*> layer;
};
