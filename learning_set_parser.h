#pragma once
#include "neuro_net.h"
#include <QTextStream>
#include <QFile>

class LearningSetParser
{
public:
    LearningSetParser(const std::string& learningSetFileName);
    void Close();
    bool GetNextLearningData(NeuroNet::LearingData& data);
    int GetLearningDataSize();
private:
    QFile m_inputFile;
    QTextStream* m_in;
    int m_rows;
    int m_columns;
};
