#include "neuro_layer.h"

NeuroLayer::NeuroLayer(int neuronsCount)
{
    for(int i = 0; i < neuronsCount; ++i)
    {
        layer.push_back(new Neuron(""));
    }
}
NeuroLayer::NeuroLayer(std::vector<std::string> classes)
{
    for(size_t i = 0; i < classes.size(); ++i)
    {
        layer.push_back(new Neuron(classes[i]));
    }

}
std::vector<double> NeuroLayer::Process(const std::vector<double>& inputs)
{
    for (auto& neuron: layer)
    {
        neuron->SetInputs(inputs);
        neuron->RandomizeWeightsIfNeed();
    }

    std::vector<double> output;

    for (auto& neuron: layer)
    {
        output.push_back(neuron->Out());
    }

    return output;
}

std::vector<std::vector<double>> NeuroLayer::Learn(const std::string& objectClass)
{
    std::vector<std::vector<double>> errorsMulOnWeight;
    std::vector<double> errors;
    double error = 0;
    double idealOut = 0;

    for (const auto& neuron: layer)
    {
        idealOut = objectClass == neuron->Name() ? 0.95 : 0;
        error = (idealOut - neuron->SavedOut()) * neuron->SavedOut() * (1 - neuron->SavedOut());
        neuron->SetError(error);
        errors.push_back(error);

        errorsMulOnWeight.push_back(neuron->MulWeightsOnError(error));
    }

    return errorsMulOnWeight;
}

std::vector<std::vector<double>> NeuroLayer::Learn(const std::vector<std::vector<double>>& prevLayoutErrors)
{
    std::vector<std::vector<double>> errorsMulOnWeight;
    std::vector<double> errors;
    double error = 0;

    for (int i = 0; i < layer.size(); ++i)
    {
        double summatorResult = 0;
        for (const auto& prevNeuronErrors: prevLayoutErrors)
        {
            summatorResult += prevNeuronErrors[i];
        }
        error = summatorResult * layer[i]->SavedOut() * (1 - layer[i]->SavedOut());
        layer[i]->SetError(error);
        errors.push_back(error);

        errorsMulOnWeight.push_back(layer[i]->MulWeightsOnError(error));
    }

    return errorsMulOnWeight;
}

void NeuroLayer::ChangeAllWeights()
{
    for (auto& neuron: layer)
    {
        neuron->ChangeAllWeights();
    }
}

std::string NeuroLayer::GetRecognizedClass()
{
    auto max = std::max_element(layer.begin(), layer.end(),
                                [](Neuron* n1, Neuron* n2)
                                { return n1->SavedOut() < n2->SavedOut(); });
    return (*max)->Name();
}
