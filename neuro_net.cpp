#include "neuro_net.h"

NeuroNet::NeuroNet(std::vector<int> neuronsCounts, std::vector<std::string> classes)
{
    int layersCount = neuronsCounts.size();

    for(int i = 0; i < layersCount; ++i)
    {
        int neuronsInLayer = neuronsCounts[i];

        m_layers.push_back(new NeuroLayer(neuronsInLayer));

    }
    m_layers.push_back(new NeuroLayer(classes));
}
void NeuroNet::Process(std::vector<double> inputs)
{
    for (const auto& layer: m_layers)
    {
        inputs = std::move(layer->Process(inputs));
    }
}

void NeuroNet::Learn(std::vector<double>& inputs, const std::string& objectClass)
{
    Process(inputs);

    auto rIt = m_layers.rbegin();

    std::vector<std::vector<double>> errorsMulOnWeight = std::move((*rIt)->Learn(objectClass));

    for (++rIt; rIt != m_layers.rend(); ++rIt)
    {
        errorsMulOnWeight = std::move((*rIt)->Learn(errorsMulOnWeight));
    }

    ChangeAllWeights();
}

std::string NeuroNet::GetRecognizedClass()
{
    return m_layers.back()->GetRecognizedClass();
}

void NeuroNet::ChangeAllWeights()
{
    for (const auto& layer: m_layers)
    {
        layer->ChangeAllWeights();
    }
}
